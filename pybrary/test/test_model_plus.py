from datetime import datetime, date, timezone
from json import load
from operator import attrgetter
from warnings import warn

from pytest import raises, mark, warns

from pybrary.model_base import Model as BaseModel, clear
from pybrary.model_plus import ModelPlus as Model, select, One, Many, backup, restore

from test_model_base import First, Last, Date, Named, Age


class BaseBirth(Model):
    father = One('Man')
    mother = One('Woman')
    date = Date()

    @property
    def year(self):
        return self.date.year


class BadBirth(BaseBirth):
    # Person is from test_model_base
    child = One('Person')


class Birth(BaseBirth):
    child = One('PersonPlus')


class MultipleBirth(BaseBirth):
    attr_name = 'birth'
    children = Many('PersonPlus')


class Pet:
    def __init__(self, name):
        self.name = name
    def __repr__(self):
        return self.name

class Dog(Pet): pass
dog = Dog('Doggy')

class Cat(Pet): pass
cat = Cat('Catty')


class PersonPlus(Named):
    age = Age()
    emails = Many(str)
    phones = Many()
    pets = Many(Pet)
    dog = One(Dog)

    @property
    def father(self):
        return hasattr(self, 'birth') and self.birth.father or None

    @property
    def mother(self):
        return hasattr(self, 'birth') and self.birth.mother or None

    @property
    def twins(self):
        if hasattr(self, 'birth') and isinstance(self.birth, MultipleBirth):
            return [
                child
                for child in self.birth.children
                if child is not self
            ]
        return []

    @property
    def siblings(self):
        parents = dict(
            father = self.father,
            mother = self.mother,
        )
        return [
            b.child
            for b in Birth.select(**parents)
            if b is not self.birth
        ] + self.twins


class Man(PersonPlus):
    gender = 1

    @property
    def wife(self):
        couples = Couple.select(husband=self)
        nb = len(couples)
        if nb == 1:
            return couples[0].wife
        elif nb > 1:
            raise ValueError(f'{nb} couples found for {self.name}')

    @property
    def children(self):
        found = Birth.select(father=self)
        children = {
            b.child.first : b.child
            for b in found
        }
        found = MultipleBirth.select(father=self)
        for multi in found:
            for child in multi.children:
                children[child.first] = child
        return children


class Woman(PersonPlus):
    gender = 0

    @property
    def husband(self):
        couples = Couple.select(wife=self)
        nb = len(couples)
        if nb == 1:
            return couples[0].husband
        elif nb > 1:
            raise ValueError(f'{nb} couples found for {self.name}')

    @property
    def children(self):
        found = Birth.select(mother=self)
        children = {
            b.child.first : b.child
            for b in found
        }
        found = MultipleBirth.select(mother=self)
        for multi in found:
            for child in multi.children:
                children[child.first] = child
        return children


class Adult(PersonPlus):
    def validate(self):
        try:
            if self.age < 18:
                raise ValueError(f'Invalid Age for Adult : {self.age}')
        except AttributeError:
            warn(f"Age of {self.name} can't be determined.")


class Couple(Model):
    husband = One('Man')
    wife = One('Woman')
    start = Date()
    stop = Date()


def test_n_default():
    class Born(Model):
        first = First()
        last = Last()
        birth = Date()

    john = Born('john', 'doe', birth=date(2003,3,3))
    assert john.birth == date(2003,3,3)

    john = Born('john', 'doe')
    assert john.birth == date(2001,1,1)

    class Born(Model):
        first = First()
        last = Last()
        birth = Date(default=date(2002,2,2))

    john = Born('john', 'doe')
    assert john.birth == date(2002,2,2)

    class Born(Model):
        first = First()
        last = Last()
        birth = Date(mandatory=True)

    with raises(ValueError) as info:
        john = Born('john', 'doe')
    assert 'Attribute Born.birth is mandatory.' in str(info.value)


def test_n_property(today):
    john = PersonPlus('john', 'doe')
    Birth(
        child = john,
        date = '20020202',
    )
    assert john.name == 'John DOE'
    assert john.age == today.year - john.birth.date.year
    with raises(ValueError) as info:
        john.age = 42
    assert 'PersonPlus.age is read only.' in str(info.value)


def test_n_validate(today):
    little = PersonPlus('little', 'john')
    Birth(
        child = little,
        date = date(today.year-10, 1, 1),
    )
    assert little.age == 10

    with warns(UserWarning):
        big = Adult('big', 'john')

    with raises(ValueError) as info:
        Birth(
            child = big,
            date = date(today.year-10, 1, 1),
        )
    assert 'Invalid Age for Adult : 10' in str(info.value)


def test_n_misc_attrs():
    john = PersonPlus('john', 'doe')
    assert not list(john.pets)
    john.pets.add(dog)
    assert dog in john.pets
    john.pets.add(cat)
    assert cat in john.pets
    with raises(ValueError) as info:
        john.pets.add(42)
    assert 'Invalid PersonPlus.pets int(42) ; should be a Pet.' in str(info.value)
    with raises(ValueError) as info:
        john.dog = 42
    assert 'Invalid PersonPlus.dog int(42) ; should be a Dog.' in str(info.value)
    with raises(ValueError) as info:
        john.dog = cat
    assert 'Invalid PersonPlus.dog Cat(Catty) ; should be a Dog.' in str(info.value)
    john.dog = 'dog name'
    assert john.dog.name == 'dog name'
    john.dog = dog
    assert john.dog.name == 'Doggy'
    mary = PersonPlus('mary', 'popins', dog=dog)
    assert mary.dog.name == 'Doggy'


def test_n_relation():
    john = Man('john', 'doe')
    mary = Woman('mary', 'popins')
    Couple(
        husband = john,
        wife = mary,
    )
    assert john.wife is mary
    assert mary.husband is john


def test_n_one():
    father = Man('father', 'doe')
    mother = Woman('mother', 'doe')
    john = PersonPlus('john', 'doe')
    assert john.mother is None
    Birth(
        child = john,
        date = '20020202',
        father = father,
        mother = mother,
    )
    assert isinstance(john.birth, Birth)
    assert john.father.last == 'DOE'
    assert john.mother is mother


def test_n_many():
    father = Man('father', 'doe')
    mother = Woman('mother', 'doe')
    first = PersonPlus('first', 'doe')
    second = PersonPlus('second', 'doe')
    birth = MultipleBirth(
        date = '20020202',
        father = father,
        mother = mother,
    )
    birth.children.add(first)
    birth.children.add(second)
    assert first in birth.children
    assert second in birth.children
    assert isinstance(first.birth, MultipleBirth)
    assert isinstance(second.birth, MultipleBirth)
    assert first.birth is second.birth
    assert first.father.last == 'DOE'
    assert second.mother is first.mother
    assert second.mother is mother
    assert first.birth.date == date(2002,2,2)
    assert first in second.twins
    assert first.twins == [second]
    assert mother.children == dict(First=first, Second=second)
    assert mother.children == father.children


def test_n_many_type():
    john = PersonPlus('john', 'doe')
    john.emails.add('a@b.c')
    with raises(ValueError) as info:
        john.emails.add(123)
    assert 'Invalid PersonPlus.emails int(123) ; should be a str.' in str(info.value)
    john.phones.add(123456789)
    john.phones.add('+33 123456789')


def test_n_select_shared_model():
    clear(BaseModel)
    john = Man('john', 'doe')
    bob = PersonPlus('bob', 'doe')
    Birth(child=bob, father=john)
    tim = PersonPlus('tim', 'doe')
    Birth(child=tim, father=john)
    joe = Man('joe', 'popins')
    mary = Woman('mary', 'popins')
    Couple(husband=joe, wife=mary)
    found_base = select(BaseModel, last='DOE')
    assert found_base
    found = select(PersonPlus, last='DOE')
    assert found == found_base
    names = [p.first for p in found]
    assert sorted(names) == ['Bob', 'John', 'Tim']
    found = select(PersonPlus, last='POPINS')
    names = [p.first for p in found]
    assert sorted(names) == ['Joe', 'Mary']
    found = select(Man, wife_first='Mary')
    names = [m.first for m in found]
    assert names == ['Joe']
    found = select(Named, father_last='DOE', first='Bob')
    names = [p.name for p in found]
    assert names == ['Bob DOE']


def test_n_backup():
    clear(BaseModel)
    backup(BaseModel, '/tmp/empty.pkl')
    assert not PersonPlus.instances

    john = Man('john', 'doe')
    bob = PersonPlus('bob', 'doe')
    Birth(child=bob, father=john)
    tim = PersonPlus('tim', 'doe')
    Birth(child=tim, father=john)
    joe = Man('joe', 'popins')
    mary = Woman('mary', 'popins')
    Couple(husband=joe, wife=mary)

    before = sorted(PersonPlus.instances, key=attrgetter('name'))
    backup(BaseModel)
    clear(BaseModel)
    assert not PersonPlus.instances
    restore(BaseModel)
    after = sorted(PersonPlus.instances, key=attrgetter('name'))

    for b, a in zip(before, after):
        assert str(a) == str(b)


'''
    These tests pass in Python, but fail every other time in Pytest ! ? !
'''

@mark.skip('broken in pytest')
def test_n_str():
    clear(BaseModel)
    john = Man('john', 'doe')
    assert str(john) == "Man(first='John', last='DOE')"
    print(' .', end='')


@mark.skip('broken in pytest')
def test_f_json():
    clear(BaseModel)
    mary = Woman('mary', 'popins')
    expected = dict(
        first = 'Mary',
        last = 'POPINS',
    )
    dumped = mary.json('/tmp/mary')
    assert dumped == expected  # use utils.sorted_dict if needed
    loaded = load(open('/tmp/mary.json'))
    assert loaded == expected  # use utils.sorted_dict if needed
    print(' .', end='')


@mark.skip('broken in pytest')
def test_f_export():
    clear(BaseModel)
    john = Man('john', 'doe')
    bob = PersonPlus('bob', 'doe')
    Birth(child=bob, father=john)
    tim = PersonPlus('tim', 'doe')
    Birth(child=tim, father=john)
    joe = Man('joe', 'popins')
    mary = Woman('mary', 'popins')
    Couple(husband=joe, wife=mary)
    expected = load(open('export.json'))
    exported = Model.export('/tmp/export')
    assert exported == expected  # use utils.sorted_dict if needed
    loaded = load(open('/tmp/export.json'))
    assert loaded == expected  # use utils.sorted_dict if needed
    print(' .', end='')


def broken_in_pytest():
    test_n_str()
    test_f_json()
    test_f_export()
    print()


if __name__ == '__main__':
    broken_in_pytest()
