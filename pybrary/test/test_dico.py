from pickle import dumps, loads

from pytest import raises

from pybrary import Dico


def test_u_():
    d = Dico()
    d['a'] = 42
    assert d.a==42


def test_s_attr():
    d = Dico()
    d[1][2] = 42
    assert d[1][2]==42


def test_s_init_kw():
    d = Dico(a=4, b=2)
    assert d.a == 4
    assert d.b == 2


def test_s_init_dict():
    initial = dict(a=4, b=2)
    d = Dico(initial)
    assert d.a == 4
    assert d.b == 2


def test_n_init_nested():
    initial = dict(
        a = 4,
        b = 2,
        c = dict(
            d = 42,
        ),
    )
    d = Dico(initial)
    assert d.a == 4
    assert d.b == 2
    assert isinstance(d.c, Dico)
    assert d.c.d == 42


def test_n_():
    d = Dico()
    d[1][2] = 42
    assert d[1]=={2:42}
    assert d[1][2]==42
    assert d[2]=={}

    d['a'] = 56
    assert d['a']==56
    assert d.a==56
    assert d.b.c=={}

    with raises(AttributeError):
        d._foo


def test_n_dict():
    d = Dico()
    d.c.b = dict(a=42)
    assert isinstance(d.c.b, Dico)
    assert d.c.b.a == 42


def test_a_subclass():
    class Foo(Dico): pass
    f = Foo()
    assert not f.a
    f.a = 42
    assert f.a == 42

    class Bar(Dico):
        def __init__(self, baz):
            super().__init__()
            self.baz = baz
    b = Bar('baz')
    assert not b.a
    b.a = 42
    assert b.a == 42


def test_a_pickle():
    d1 = Dico()
    d1.a.b.c = 'abc'
    d1._foo = 'foo'
    p = dumps(d1)

    d2 = loads(p)
    assert d2.a.b.c == 'abc'
    assert d2 == d1
    assert d1._foo == 'foo'
    with raises(AttributeError):
        d2._foo


def test_c_():
    d = Dico()
    d.functions.double = lambda x: 2*x
    d.functions.square = lambda x: x*x
    assert isinstance(d.functions, dict)
    assert list(d.functions.keys())=='double square'.split()
    assert d.functions.double(3)==6
    assert d.functions['square'](3)==9

def test_f_():
    d = Dico()
    assert isinstance(d, dict)
    d.a.b = 42
    assert isinstance(d, dict)
    assert isinstance(d.a, dict)
    assert isinstance(d['a'], dict)
    assert d.a==d['a']
    assert d.a=={'b':42}
    assert d.a.b==42
    assert d.a['b']==42
    assert d['a']['b']==42
    assert d['a'].b==42
    assert d==dict(d)

def test_f_dump():
    pkl = '/tmp/dico.plk'
    d1 = Dico()
    d1.a = 1
    d1.b.b = 2
    d1.dump(pkl)

    d2 = Dico()
    d2.load(pkl)
    assert d2==d1

    class Foo(Dico): pass
    f1 = Foo()
    f1.bar.baz = 42
    f1.dump(pkl)

    f2 = Foo()
    f2.load(pkl)
    assert f2.bar.baz == 42

    d3 = Dico()
    d3.load(pkl)
    assert d3 == f1

def test_f_str():
    d = Dico(a=42, b=(1,2,3))
    expected = '''{
    a = 42,
    b = (
        1,
        2,
        3,
    ),
}
'''
    assert str(d) == expected, f'\n:{str(d)}:\n:{expected}:'

