from datetime import datetime, timezone, timedelta

from pytest import fixture


now = datetime.now(tz=timezone(timedelta(0)))


@fixture(autouse=True)
def today():
    return now


