from pybrary import Rex, Patterns, Parser


def test_n_rex_match():
    rex = Rex(r't.t')
    assert rex.match('toto')
    assert rex.match('titi')
    assert rex.match('totoz')
    assert not rex.match('topo')
    assert not rex.match('atoto')
    rex = Rex(r't.t$')
    assert not rex.match('toto')


def test_n_rex_isin():
    rex = Rex(r't.t')
    assert rex.isin('toto')
    assert rex.isin('titi')
    assert rex.isin('totoz')
    assert rex.isin('atoto')
    assert not rex.isin('topo')


def test_n_rex_find():
    rex = Rex(r't.t')
    assert rex.find('ateti') == 'tet'


def test_n_rex_findall():
    rex = Rex(r't.t')
    assert rex.findall('toto titi') == ['tot', 'tit']


def test_n_rex_split():
    rex = Rex(r't.t')
    assert rex.split('totonetittwo') == ['one', 'two']


def test_n_rex_replace():
    rex = Rex(r't.t')
    assert rex.replace('123', 'one titi two ututu') == 'one 123i two u123u'


def test_n_rex_subfct():
    def dbl(txt):
        return txt*2

    rex = Rex(r't.t')
    rex.switch=dbl
    assert rex.subfct('toto titi') == 'tottoto tittiti'


def test_n_rex_groups():
    rex = Rex(r'_(\d{3})(\d{3})_')
    txt = 'abc_123456_xyz'
    assert rex.findall(txt) == ['123', '456']
    assert rex.replace(r'_\1sep\2_', txt) == 'abc_123sep456_xyz'


def test_f_rex():
    rex = Rex(r'\(o\)(to)')
    txt = 't(o)to p(o)tola'
    assert rex.find(txt) == '(o)to'
    assert rex.findall(txt) == ['to', 'to']

    txt = '66_jsf.jsf, a45_sfjk. b567_toto.'
    rex = Rex(r'''
        .*              # qqch ou rien (greedy)
        (\d{2,3})       # 2 ou 3 chiffres
        _               # le sep
        (\w+)           # au moins 1 "w"
        \.              # et un .
    ''')
    assert rex.find(txt) == '66_jsf.jsf, a45_sfjk. b567_toto.'
    assert rex.findall(txt) == ['67', 'toto']

    rex = Rex(r'''
        .*?             # qqch ou rien (not greedy)
        (\d{2,3})       # 2 ou 3 chiffres
        _               # le sep
        (\w+)           # au moins 1 "w"
        \.              # et un .
    ''')
    assert rex.find(txt) == '66_jsf.'
    assert rex.findall(txt) == ['66', 'jsf', '45', 'sfjk', '567', 'toto']


def test_n_patterns():
    pat = Patterns()
    assert pat.find('whatever') == True
    pat('what.*')
    assert pat.find('whatever') == True
    pat('ever$')
    assert pat.find('whatever') == True
    assert pat.findall('whatever') == True
    assert pat.findany('whatever') == True
    pat('what.*', match=False)
    assert not pat.find('whatever') == True
    pat = Patterns('toto')
    assert not pat.find('whatever') == True
    pat = Patterns('toto', match=False)
    assert pat.find('whatever') == True


def test_n_parser():
    parser = Parser([
        ('INTEGER',     r'[0-9]+'),
        ('IDENTIFIER',  r'[a-z_]+'),
        ('PUNCTUATION', r'[,.]+'),
        (None,          r'\s+'), # None == skip token.
    ])
    tokens = parser.parse('45 pigeons, 23 cows, 11 spiders.')
    assert tokens == [
        ('INTEGER', '45'), ('IDENTIFIER', 'pigeons'), ('PUNCTUATION', ','),
        ('INTEGER', '23'), ('IDENTIFIER', 'cows'), ('PUNCTUATION', ','),
        ('INTEGER', '11'), ('IDENTIFIER', 'spiders'), ('PUNCTUATION', '.')
    ]

