from pathlib import Path

from pytest import MonkeyPatch, warns

from pybrary import Config


app = 'test_app_config'
defaults = dict(
    a = 1,
    b = 'b',
    c = r'a\b',
)

def check_defaults(config):
    for k, v in defaults.items():
        assert getattr(config, k) == v

def check_environ(config):
    attr, value = 'a', 1
    assert getattr(config, attr) == value
    name, value = f'{app}_{attr}', 42
    with MonkeyPatch().context() as ctx:
        # NOTE: values in environ are str
        with warns(UserWarning) as w:
            ctx.setenv(name, value)
        assert getattr(config, attr) == str(value)

def clear():
    conf = Path(f'~/.config/{app}').expanduser()
    for path in conf.glob('config*'):
        path.unlink()


def test_u_py():
    clear()
    config = Config(app)
    assert config.missing == None
    assert config.path.suffix == '.py'


def test_u_toml():
    clear()
    config = Config(app, ext='toml')
    assert config.missing == None
    assert config.path.suffix == '.toml'


def test_u_yaml():
    clear()
    config = Config(app, ext='yaml')
    assert config.missing == None
    assert config.path.suffix == '.yaml'


def test_u_json():
    clear()
    config = Config(app, ext='json')
    assert config.missing == None
    assert config.path.suffix == '.json'


def test_u_def_py():
    clear()
    config = Config(app, defaults=defaults)
    assert config.path.suffix == '.py'
    check_defaults(config)
    check_environ(config)


def test_u_def_toml():
    clear()
    config = Config(app, defaults=defaults, ext='toml')
    assert config.path.suffix == '.toml'
    check_defaults(config)
    check_environ(config)


def test_u_def_yaml():
    clear()
    config = Config(app, defaults=defaults, ext='yaml')
    assert config.path.suffix == '.yaml'
    check_defaults(config)
    check_environ(config)


def test_u_def_json():
    clear()
    config = Config(app, defaults=defaults, ext='json')
    assert config.path.suffix == '.json'
    check_defaults(config)
    check_environ(config)
