from typing import List, Dict, Tuple, Callable


from pybrary import Tracer, trace, set_tracer
from tracing import tracer


def add(a, b):
    print('adding ...')
    return a+b

class Adder:
    def __call__(self, x, y:int = 33) -> int:
        from time import sleep
        sleep(3.456)
        try:
            return add(x, y)
        except Exception:
            pass

def callee(a=1, b=2):
    c = add(a, b)
    print(f'{c=}')
    c = add(33, b=55)
    print(f'{c=}')
    a = Adder()
    c = a(4, '7')
    c = a(4, 7)
    c = a(3.3)
    return c

def types():
    def one(a:int, b) -> str: return a+b

    def two(
        s: str,
        c: Callable,
        f: Callable[..., Tuple[str, int, Dict[str, str]]],
        t: Tuple,
        l: List[str],
    ) -> None:
        one(1, 2)

    two('a', one, one, ['d',], ['e'])


@trace
def traced(x):
    add(4, x)

@trace
def call_traced(x=7):
    traced(x)

@trace
def call_2():
    add(5, 5)
    traced(5)
    call_traced(5)


class Computer:
    @trace
    def add(self, a, b=0):
        return a+b

    @trace
    def mul(self, a, b=1):
        r = 0
        for i in range(b):
            r = self.add(r, a)
        return r

def test_tracer():
    with Tracer(
        # show_types  = True,
        # check_types = True,
        # show_times  = True,
        # show_stamp  = True,
        # buffered    = False,
        label       = 'callee',
        # dest        = '/tmp',
        quiet       = False,
        echo        = True,
    ):
        c = callee()

    with Tracer(
        show_types  = True,
        check_types = True,
        show_times  = True,
        show_stamp  = True,
        buffered    = False,
        label       = 'types',
        # dest        = '/tmp',
        quiet       = False,
    ):
        types()

    with tracer('local_tracer'):
        add(1, 2)
        add(3, 4)

    with tracer('local_tracer'):
        types()
        add(5, 6)
        add(7, 8)

def test_trace():
    traced(2)
    traced()
    call_traced()
    call_2()

    set_tracer(label='Compute', quiet=False)
    c = Computer()
    c.add(7, 8)
    c.mul(3, 3)

def test_format_arg():
    xml_data = 'some prefix<?xml ... data ...>'
    xml_dict = {'some key': 1, 'Dataset': 2}
    frame = {'frame': 3, 'dict': 4}
    @trace
    def process(arg, data):
        return xml_dict
    process(5, xml_data)
    process(7, frame)


if __name__ == '__main__': test_format_arg()
