from pybrary import Flags, sorted_dict


def test_s_flags():
    flags = Flags('one', 'two', 'three')
    assert flags.one == 1
    assert flags.two == 2
    assert flags.three == 4


def test_n_flags_expose():
    flags = Flags('one', 'two', 'three')
    flags.expose()
    assert one == 1
    assert two == 2
    assert three == 4


def test_s_sorted_dict():
    a = dict(
        a = [1,2,3],
        b = [4,5,6],
        c = [7,8,9],
    )
    b = dict(
        b = [6,5,4],
        c = [7,8,9],
        a = [1,3,2],
    )
    assert a != b
    assert sorted_dict(a) == sorted_dict(b)

