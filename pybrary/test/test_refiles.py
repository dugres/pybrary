﻿from pathlib import Path
from time import perf_counter as time

from pybrary.files import walk
from pybrary.refiles import Matcher, _grep1, _grep2a, _grep2b


def test_n_match():
    m = Matcher(ext=r'py$')
    m.path(r'/usr/share/.*python.*')
    m.name(r'__init__', match=False)
    m.cont(r'import')
    m.cont(r'from')
    m.cont(r'class', match=False)
    m.cont(r'def')
    found = False
    for f in walk('/usr/share/python3'):
        if m.match(f):
            found = True
            assert str(f).startswith('/usr/share')
            assert 'python' in str(f)
            assert f.suffix == '.py'
            assert f.name != '__init__.py'
            cont = f.read_text()
            assert 'import' in cont
            assert 'from' in cont
            assert 'class' not in cont
            assert 'def' in cont
    assert found


def test_n_match_not():
    m = Matcher(
        ext = r'py$',
        not_name = r'__init__',
        not_cont = r'class',
    )
    m.path(r'/usr/share/.*python.*')
    m.cont(r'import')
    m.cont(r'from')
    m.cont(r'def')
    found = False
    for f in walk('/usr/share/python3'):
        if m.match(f):
            found = True
            assert str(f).startswith('/usr/share')
            assert 'python' in str(f)
            assert f.suffix == '.py'
            assert f.name != '__init__.py'
            cont = f.read_text()
            assert 'import' in cont
            assert 'from' in cont
            assert 'class' not in cont
            assert 'def' in cont
    assert found


def test_n_find():
    m = Matcher(ext=r'py$')
    m.path(r'/usr/share')
    m.name(r'test_', match=False)
    m.cont(r'import')
    m.cont(r'\wclass\w.*:')
    m.cont(r'class.+(object)', match=False)
    found = False
    root = Path('/usr/share/doc')
    for f in m.find(root):
        found = True
        assert f.endswith('.py')
        assert 'test_' not in f
        cont = (root/f).read_text()
        assert 'import' in cont
        assert 'class' in cont
        assert '(object)' not in cont
    assert found


def test_grep1():
    m = Matcher(cont='abc')
    lines = [
        'abc 1',
        'xxx 2',
        'abc 3',
    ]
    assert list(_grep1(lines, m)) == [(1, 'abc 1'), (3, 'abc 3')]


def test_grep12():
    m = Matcher(cont='abc')
    lines = [
        'abc 1',
        'xxx 2',
        'abc 3',
    ]
    # print()
    for grep2 in (_grep2a, _grep2b):
        start = time()
        result = list(grep2(lines, 0, 0, m))
        total = time() - start
        assert list(_grep1(lines, m)) == result
        # print(f'{grep2.__name__} : {round(total*1E6):>3} us')


def idx(gen): return [i[0] for i in gen]

def test_grep2a():
    m = Matcher(cont='abc')
    lines = [
        'xxx 1',
        'xxx 2',
        'abc 3',
        'xxx 4',
        'xxx 5',
    ]
    # print()
    for grep in (_grep2a, _grep2b):
        start = time()
        assert idx(grep(lines, 0, 0, m)) == [3]
        assert idx(grep(lines, 1, 0, m)) == [2, 3]
        assert idx(grep(lines, 0, 1, m)) == [3, 4]
        assert idx(grep(lines, 1, 1, m)) == [2, 3, 4]
        assert idx(grep(lines, 2, 0, m)) == [1, 2, 3]
        assert idx(grep(lines, 0, 2, m)) == [3, 4, 5]
        assert idx(grep(lines, 2, 2, m)) == [1, 2, 3, 4, 5]
        total = time() - start
        # print(f'{grep.__name__} : {round(total*1E6):>3} us')


def test_grep2b():
    m = Matcher(cont='abc')
    lines = [
        'abc 1',
        'xxx 2',
        'abc 3',
        'xxx 4',
        'abc 5',
    ]
    # print()
    for grep in (_grep2a, _grep2b):
        start = time()
        assert idx(grep(lines, 0, 0, m)) == [1, 3, 5]
        assert idx(grep(lines, 1, 0, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 0, 1, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 1, 1, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 2, 0, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 0, 2, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 2, 2, m)) == [1, 2, 3, 4, 5]
        total = time() - start
        # print(f'{grep.__name__} : {round(total*1E6):>3} us')


def test_grep2c():
    m = Matcher(cont='abc')
    lines = [
        'xxx 1',
        'abc 2',
        'xxx 3',
        'abc 4',
        'xxx 5',
    ]
    # print()
    for grep in (_grep2a, _grep2b):
        start = time()
        assert idx(grep(lines, 0, 0, m)) == [2, 4]
        assert idx(grep(lines, 0, 1, m)) == [2, 3, 4, 5]
        assert idx(grep(lines, 1, 0, m)) == [1, 2, 3, 4]
        assert idx(grep(lines, 1, 1, m)) == [1, 2, 3, 4, 5]
        assert idx(grep(lines, 0, 2, m)) == [2, 3, 4, 5]
        assert idx(grep(lines, 2, 0, m)) == [1, 2, 3, 4]
        assert idx(grep(lines, 2, 2, m)) == [1, 2, 3, 4, 5]
        total = time() - start
        # print(f'{grep.__name__} : {round(total*1E6):>3} us')


def test_grep2d():
    m = Matcher(cont='abc')
    lines = [
        'xxx 1',
        'xxx 2',
        'xxx 3',
        'abc 4',
        'xxx 5',
        'xxx 6',
        'xxx 7',
        'xxx 8',
        'abc 9',
        'xxx 10',
        'xxx 11',
        'xxx 12',
    ]
    # print()
    for grep in (_grep2a, _grep2b):
        start = time()
        assert idx(grep(lines, 0, 0, m)) == [4, 9]
        assert idx(grep(lines, 0, 1, m)) == [4, 5, 9, 10]
        assert idx(grep(lines, 1, 0, m)) == [3, 4, 8, 9]
        assert idx(grep(lines, 1, 1, m)) == [3, 4, 5, 8, 9, 10]
        assert idx(grep(lines, 0, 2, m)) == [4, 5, 6, 9, 10, 11]
        assert idx(grep(lines, 2, 0, m)) == [2, 3, 4, 7, 8, 9]
        assert idx(grep(lines, 2, 2, m)) == [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        total = time() - start
        # print(f'{grep.__name__} : {round(total*1E6):>3} us')

