from pybrary.httplib import RequestHandler, serve, request


class Handler(RequestHandler):
    routes = (
        ('get', '/hi', 'say_hi'),
    )

    def say_hi(self):
        return 200, 'Hi !'


def test_n_http_serve():
    adr, port = 'localhost', 8888
    with serve(Handler, adr=adr, port=port):
        code, text = request(f'http://{adr}:{port}/hi')
        assert code == 200
        assert text == 'Hi !'

    with serve(Handler, adr=adr, port=port):
        code, text = request(f'http://{adr}:{port}/hi')
        assert code == 200
        assert text == 'Hi !'

        code, text = request(f'http://{adr}:{port}/hi')
        assert code == 200
        assert text == 'Hi !'


if __name__=='__main__': test_n_http_serve()
