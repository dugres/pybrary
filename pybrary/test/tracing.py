from pybrary.tracer import Tracer, trace, set_tracer


tracers = dict()


def fstr(s):
    x = s.find('<?xml')
    if x>=0:
        s = s[:x] + '<-XML-data->'
    return s

def fdict(d):
    for k in d:
        if 'Dataset' in k: return '<-XML-dict->'
    return str(d)

def frame(f):
    return fstr(str(f))

fnames = dict(
    frame = frame,
)

ftypes = {
    'str': fstr,
    'dict': fdict,
}

def make_tracer(label='tracing'):
    tracer = Tracer(
        label = label,
        dest = '/tmp/log',
        # show_stamp = True,
        show_types = False,
        buffered = False,
        echo = False,
        quiet = False,
        # full_path = True,
        format_arg_names = fnames,
        format_arg_types = ftypes,
    )
    tracer.truncate_path('/usr/src/app')
    tracer.ignore_path('logger')
    tracer.ignore_path('tracing')
    tracer.ignore_path('werkzeug')
    tracer.ignore_func('tracer')
    return tracer


def tracer(label='tracing'):
    tracer = tracers.get(label)
    if not tracer:
        tracer = make_tracer(label)
        tracers[label] = tracer
    return tracer


set_tracer(make_tracer('trace'))


