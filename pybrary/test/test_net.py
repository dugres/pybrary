from pybrary import get_ip_adr


def test_n_iget_ip_adr():
    bak = None
    for _ in range(3):
        ok, adr = get_ip_adr()
        assert ok
        if not bak: bak = adr
        assert adr == bak, f'{adr} ! {bak}'


if __name__ == '__main__': test_n_iget_ip_adr()
