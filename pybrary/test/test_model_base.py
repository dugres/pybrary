from datetime import datetime, date, timezone

from pytest import raises

from pybrary.model_base import Attribute, Property, Model

from pybrary.test.conftest import now


class First(Attribute):
    def validate(self, val):
        if isinstance(val, str) and val.isalpha():
            return val.capitalize()


class Last(Attribute):
    mandatory = True

    def validate(self, val):
        if isinstance(val, str) and val.isalpha():
            return val.upper()


class Name(Property):
    def get(self, obj):
        return f'{obj.first} {obj.last}'


class Date(Attribute):
    default = date(2001,1,1)
    fmt = '%Y%m%d'

    def validate(self, val):
        if isinstance(val, date):
            return val

        if isinstance(val, str):
            return datetime.strptime(val, self.fmt).date()

    def dump(self, val):
        return val.strftime(self.fmt)


class Age(Property):
    def get(self, obj):
        if obj.birth:
            return now.year - obj.birth.year


class Named(Model):
    first = First()
    last = Last()
    name = Name()


class Person(Named):
    birth = Date()
    age = Age()


def test_s_model():
    john = Named('john', 'doe')
    assert john.first == 'John'
    assert john.last == 'DOE'


def test_n_validate():
    with raises(ValueError) as info:
        john = Named(123, 'doe')
    assert 'Invalid Named.first int(123).' in str(info.value)


def test_n_mandatory():
    john = Named(last='doe')
    assert john.first is None
    assert john.last == 'DOE'

    with raises(ValueError) as info:
        john = Named('john')
    assert 'Attribute Named.last is mandatory.' in str(info.value)


def test_n_default():
    john = Person('john', 'doe', birth=date(2003,3,3))
    assert john.birth == date(2003,3,3)

    john = Person('john', 'doe')
    assert john.birth == date(2001,1,1)

    class Born(Model):
        first = First()
        last = Last()
        birth = Date(default=date(2002,2,2))

    john = Born('john', 'doe')
    assert john.birth == date(2002,2,2)

    class Born(Model):
        first = First()
        last = Last()
        birth = Date(mandatory=True)

    with raises(ValueError) as info:
        john = Born('john', 'doe')
    assert 'Attribute Born.birth is mandatory.' in str(info.value)


def test_n_property(today):
    john = Person('john', 'doe')
    assert john.name == 'John DOE'
    assert john.age == today.year - john.birth.year
    with raises(ValueError) as info:
        john.age = 42
    assert 'Person.age is read only.' in str(info.value)

