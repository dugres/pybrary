from pybrary.ascii import rm_ansi_codes, oct_mod, clean_name


def test_s_oct_mod():
    assert oct_mod('r--r--r--') == '444'


def test_s_rm_ansi():
    esc, color, txt = chr(27), 33, 'OK'
    colored = f'{esc}[1;{color}m{txt}{esc}[0m'
    assert rm_ansi_codes(colored) == 'OK'

    colored = r'[H[2J[3JOK'
    assert rm_ansi_codes(colored) == 'OK'


def test_s_clean_name():
    assert clean_name('Hello World !') == 'Hello_World'


if __name__=='__main__': test_s_rm_ansi()
