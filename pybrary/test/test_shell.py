from pybrary import bash, pipe


def test_s_shell():
    ret, out, err = bash('ls -l')
    assert isinstance(ret, int)
    assert ret==0
    assert isinstance(out, str)
    assert out
    assert isinstance(err, str)
    assert not err


def test_n_shell_err_arg():
    ret, out, err = bash('ls -l not_there')
    assert isinstance(ret, int)
    assert ret==2
    assert isinstance(out, str)
    assert not out
    assert isinstance(err, str)
    assert err


def test_n_shell_err_cmd():
    ret, out, err = bash('dum')
    assert isinstance(ret, int)
    assert ret==127
    assert isinstance(out, str)
    assert not out
    assert isinstance(err, str)
    assert err


def test_s_shell_script():
    ret, out, err = bash('''
        pwd
        ls -l
        cd /tmp
        pwd
        ls -l
        cd -
        pwd
        ls -l
    ''')
    assert isinstance(ret, int)
    assert ret==0
    assert isinstance(out, str)
    assert out
    assert isinstance(err, str)
    assert not err


def test_s_shell_script_err():
    ret, out, err = bash('''
        pwd
        ls -l
        cd /dum
        pwd
        dum
        cd -
        pwd
        ls -l
    ''')
    assert isinstance(ret, int)
    assert ret==0
    assert isinstance(out, str)
    assert out
    assert isinstance(err, str)
    assert err


def test_n_pipe():
    for line in pipe(
        'find /usr -type f',
        'grep -i python',
        'grep -v pyc',
    ):
        assert line.startswith('/usr')
        assert 'python' in line.lower()
        assert 'pyc' not in line


if __name__=='__main__': test_n_pipe()
