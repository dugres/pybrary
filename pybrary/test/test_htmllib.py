from pybrary.htmllib import Parser, Node


def test_n_parser():
    url = "https://httpbin.org/html"
    root = Parser().get(url)
    for node in root:
        assert isinstance(node, Node)
        assert str(node).strip().startswith(node.tag)
        assert node.classes == ['']
        assert node.attrs == {}
        assert isinstance(node.parent, Node)
        assert node.level == node.parent.level + 1
        for child in node.children:
            assert isinstance(child, Node)
            assert child.parent is node
            assert child.level == node.level +1
        assert isinstance(node.text, str)


if __name__=='__main__': test_n_parser()
