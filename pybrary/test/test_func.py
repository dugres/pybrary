from pytest import raises

from pybrary.func import (
    caller,
    todo,
    memo,
    singleton,
)


def callee():
    return caller()

def test_n_caller():
    assert callee() == 'test_func.test_n_caller'


def test_n_todo():
    class AbsCls:
        def abs_meth(self): todo(self)

    a = AbsCls()
    with raises(NotImplementedError) as x:
        a.abs_meth()
    assert str(x.value).strip() == '! abs_meth missing in AbsCls !'

def test_n_memo():
    class MemoTest:
        @memo
        def prop(self):
            return 'value'

    c = MemoTest()
    assert not hasattr(c, '_memo_prop')
    assert c.prop == c._memo_prop


@singleton
class One:
    def __init__(self, a):
        self.a = a


def test_n_singleton():

    first = One(1)
    assert first.a == 1

    second = One(2)
    assert second.a == 1

    assert second is first


if __name__=='__main__': test_n_memo()
