from pybrary.fernet import Fernet


def test_text():
    origin = 'Hello world'
    passwd = 'azerty'
    fernet = Fernet(passwd)
    secret = fernet.encrypt(origin)
    result = fernet.decrypt(secret)
    assert result == origin


def test_binary():
    origin = b'Hello world'
    passwd = 'azerty'
    fernet = Fernet(passwd)
    secret = fernet.encrypt(origin, text=False)
    result = fernet.decrypt(secret, text=False)
    assert result == origin
